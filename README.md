# Twitch Client - M�rcio Correia

_(Requirements and Features listed on bottom)_

##Usage

In the project directory, before usage, you should run:

#### `npm install`

---

When previous step is complete, run:

#### `npm start`

This command runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

---

To run the project tests, run: (06-04-2019 - Now Available)
#### `npm test`

This command launches the test runner in the interactive watch mode.<br>

---
#### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!


## Features & Requirements

##### Functional Requirements
* Allow the user to search for streams;  **_Checked !_**
* Allow the user to choose the number of results when performing a search **_Checked !_**
* Remember this value even when the browser is closed. **_Checked !_**
* Allow the user to open a stream from the search results; **_Checked !_**
* Keep the the viewers count updated;

**Extra Feature Added** : Pagination / Navigation to other pages

##### Extras Application:
* Cross Browser compatibility with modern Web Browsers. **_Checked !_**
* Usage feedback (e.g. loading state, error handling); **_Checked !_**
* Performance aware (e.g. don't overload the Twitch service); **_Checked !_**
* Incremental Search;
* Responsive UI;**_Checked !_**
* Routing;**_Checked !_**

##### Development:
* Use some kind of style sheet;**_Checked !_**
* Use build tools (e.g. grunt, gulp, webpack) **_Checked !_**
* Different environments for development and production 
* Unit tests  
* Functional tests **_Checked_**
* Clean Code **_Guess is Checked :-)_**
* Amaze us **_Tried :-)_**
