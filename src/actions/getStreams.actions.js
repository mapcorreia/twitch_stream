import twitchApiURL from '../helpers/axios_api'
import { clientId } from '../helpers/apiClientId'
import { GET_STREAM_LIST, SEARCH_STREAM } from '../helpers/constants'

const headers = { 'Client-ID': clientId }

export const getStreamList = (results, queryParams) => {
    return async (dispatch) => {
        try {
            const response = await twitchApiURL.get(`/helix/streams?first=${results}&${queryParams}`, { headers })
            dispatch({ type: GET_STREAM_LIST, payload: response.data })

        } catch (e) {
            console.log(e)
        }
    }
}

export const searchStream = (term,results) => {
    return async (dispatch) => {
        try {
            const response = await twitchApiURL.get(`/kraken/search/streams?query=${term}&limit=${results}`, { headers })
            dispatch({ type: SEARCH_STREAM, payload: response.data })

        } catch (e) {
            console.log(e)
        }
    }
}