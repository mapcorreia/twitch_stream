import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom'
import StreamList from './Components/StreamList/StreamList'
import StreamShow from './Components/StreamShow/StreamShow'
import {Header} from './Components/Header/Header'

class App extends Component {

  render() {
    return (
      <div >
        <BrowserRouter>
          <div>
            <Header />
            <Route path="/" exact component={StreamList} />
            <Route path="/streams/show/:id" exact component={StreamShow} />
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
