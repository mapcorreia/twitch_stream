import React from 'react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { render, cleanup } from 'react-testing-library'

import App from '../App';
import thunk from 'redux-thunk';

afterEach(cleanup)

const initialUndefinedState = {
    streamList: {
        defaultList: undefined
    },
    streamsSearched: {}
};

const initialStateWithData = {
    streamList: {
        defaultList: []
    },
    streamsSearched: {}
};

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

it('renders Loading Spinner while default list is undefined', () => {
    const store = mockStore(initialUndefinedState);
 
    const { container } = render(
        <Provider store={store}>
            <App />
        </Provider>)

    const spinner = container.querySelector('#loadingSpinner')
    expect(spinner).toBeTruthy()
   
});

it('does not renders Loading Spinner when data is fetched', () => {
    const store = mockStore(initialStateWithData);
 
    const { container } = render(
        <Provider store={store}>
            <App />
        </Provider>)

    const spinner = container.querySelector('#loadingSpinner')
    expect(spinner).toBeFalsy()

    const streamList = container.querySelector('#streamGrid')
    expect(streamList).toBeTruthy()
   
});