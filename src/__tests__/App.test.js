import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import App from '../App';
import thunk from 'redux-thunk';

const initialState = {
	streamList: {},
	streamsSearched: {}
};
const middlewares = [thunk];
const mockStore = configureStore(middlewares);

it('renders without crashing', () => {
	const store = mockStore(initialState);
	const div = document.createElement('div');
	ReactDOM.render(
		<Provider store={store}>
			<App />
		</Provider>,
		div
	);
	ReactDOM.unmountComponentAtNode(div);
});