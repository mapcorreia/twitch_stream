import { GET_STREAM_LIST, SEARCH_STREAM } from '../helpers/constants'


export const getStreamListReducer = (state, action) => {
    if (action.type === GET_STREAM_LIST) {
        return {
            defaultList: action.payload.data,
            paginationID: action.payload.pagination
        }
    } else {
        return (typeof state === 'undefined' ? [] : state);
    }
};


export const searchStreamsReducer = (state, action) => {
    if (action.type === SEARCH_STREAM) {
        return {
            streamsSearched: action.payload.streams
        }
    } else {
        return (typeof state === 'undefined' ? [] : state);
    }
};