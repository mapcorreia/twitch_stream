import { combineReducers } from 'redux';
import { getStreamListReducer, searchStreamsReducer } from './streamsReducer'

const rootReducer = combineReducers({
  streamList: getStreamListReducer,
  streamsSearched: searchStreamsReducer

});

export default rootReducer;