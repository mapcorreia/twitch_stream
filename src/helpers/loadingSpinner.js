import React from 'react'
import { ScaleLoader } from 'react-spinners'

export const LoadingSpinner = () => {
    return (
        <div id="loadingSpinner" className="loader_Style">
            <ScaleLoader
                sizeUnit={"px"}
                size={600}
                color={'#393D46'}
                loading={true}
            />
            <label>Loading...</label>
        </div>
    )
}