import React from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux'
import { Col, Row, Container, Button } from 'react-bootstrap'
import { getStreamList } from '../../actions'
import { LoadingSpinner } from '../../helpers/loadingSpinner'
import SearchBar from '../SearchBar/SearchBar'
import { StreamCard } from './StreamCard'
import './StreamList.css'

class StreamList extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            resultsPerPage: 12
        }
    }

    componentDidMount = () => {
        const storedNumbResults = localStorage.getItem('numberOfResults')
        this.setState({resultsPerPage:storedNumbResults})
        this.props.getStreamList(this.state.resultsPerPage, '')
    }

    renderGridOfStreams = (data) => {
        return (
            <Container id="streamGrid">
                <Row className="justify-content-md-center defaultList_spacing">
                    {data.map((stream, index) =>
                        <Col key={index} xs={6} sm={6} lg={4}>
                            <Link to={{
                                pathname: `/streams/show/${stream.id}`,
                                state: {
                                    title: stream.title,
                                    user_name: stream.user_name,
                                    viewer_count: stream.viewer_count,
                                    source: stream.user_name
                                }
                            }}>
                                <StreamCard preview={stream.thumbnail_url} title={stream.title} />
                            </Link>
                        </Col>
                    )}
                </Row>
            </Container>
        )
    }

    renderSearchResult = (data) => {
        return (
            <Container>
                <div className="searchResults_title">Search Results</div>
                <Row className="justify-content-md-center searchResult_spacing ">
                    {data.map((stream, index) =>
                        <Col key={index} xs={6} sm={6} lg={4}>
                            <Link to={{
                                pathname: `/streams/show/${stream.id}`,
                                state: {
                                    title: stream.channel.status,
                                    user_name: stream.channel.display_name,
                                    viewer_count: stream.channel.views,
                                    source: stream.channel.name
                                }
                            }}>
                                <StreamCard preview={stream.preview.medium} title={stream.channel.display_name} />
                            </Link>
                        </Col>
                    )}
                </Row>
            </Container>
        )
    }

    renderFooter = () => {
        return (
            <div className="pagination_buttons_div container">
                <Button
                    value={'before'}
                    className="pagination_buttons_style"
                    onClick={this.handlePagBtnClick}>
                    Previous Page
                </Button>
                <Button
                    value={'after'}
                    className="pagination_buttons_style"
                    onClick={this.handlePagBtnClick}>
                    Next Page
                </Button>
            </div>
        )
    }


    handlePagBtnClick = (e) => {
        this.props.getStreamList(this.state.resultsPerPage, `${e.target.value}=${this.props.pagination.cursor}`)
    }

    render() {
        const { defaultList,  streamsSearched } = this.props

        if (streamsSearched !== undefined) {
            return (
                <div>
                    <SearchBar />
                    {this.renderSearchResult(streamsSearched)}
                </div >
            )
        }
        else if (defaultList !== undefined) {
            return (
                <div>
                    <SearchBar />
                    {this.renderGridOfStreams(defaultList)}
                    {this.renderFooter()}
                </div >
            )
        } else {
            return (
                <div className="spinnerAlignment">
                    <LoadingSpinner />
                </div>
            )
        }

    }
}

const mapStateToProps = (state) => {
    return {
        defaultList: state.streamList.defaultList,
        pagination: state.streamList.paginationID,
        streamsSearched: state.streamsSearched.streamsSearched
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
        ...bindActionCreators({ getStreamList }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StreamList);
