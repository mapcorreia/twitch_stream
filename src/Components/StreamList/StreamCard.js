import React from 'react'
import { Card } from 'react-bootstrap'
import './StreamCard.css'

export const StreamCard = (props) => {

    const { preview, title } = props

    return (
        <Card className="card_style" >
            <Card.Header className='card_title'>{title.substring(0,25)}</Card.Header>
            <Card.Img variant="top" className="card_image" src={preview.replace('{width}x{height}', '339x160')} />
        </Card>
    )
}
