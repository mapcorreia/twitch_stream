import React from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { Button, Dropdown, DropdownButton } from 'react-bootstrap'
import './SearchBar.css'
import { searchStream } from '../../actions'


class SearchBar extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            searchInput: '',
            numberOfResults: 'Select # Results',
            showError: false
        }
    }

    componentDidMount = () => {
        const storedNumbResults = localStorage.getItem('numberOfResults')
        if (storedNumbResults !== null) {
            this.setState({ numberOfResults: storedNumbResults })
        }
    }

    handleInputChange = (e) => {
        this.setState({
            searchInput: e.target.value,
            showError: false
        })
    }

    onSearch = (e) => {
        e.preventDefault()
        const { searchInput, numberOfResults } = this.state

        if (/^\s*$/.test(searchInput) || numberOfResults === 'Select # Results') {
            this.setState({ showError: true })
        } else {
            this.props.searchStream(searchInput, numberOfResults)
        }

    }

    handleDropSelect = (e) => {
        const resultsToShowSelected = e.target.getAttribute('value')
        localStorage.setItem('numberOfResults', resultsToShowSelected)
        this.setState({ numberOfResults: resultsToShowSelected })

    }

    render() {

        const errorClass = this.state.showError === false ? 'hide' : 'error_div'

        return (
            <React.Fragment>
                <div className="searchBar container">
                    <input
                        className="inputField"
                        type='text'
                        placeholder="Search..."
                        value={this.state.searchInput}
                        onChange={this.handleInputChange} >
                    </input>
                    <DropdownButton id="dropdown-item-button" title={this.state.numberOfResults} className="dropdown_button">
                        <Dropdown.Item onClick={this.handleDropSelect} value={12} className="dropdown_Item">12</Dropdown.Item>
                        <Dropdown.Item onClick={this.handleDropSelect} value={24} className="dropdown_Item">24</Dropdown.Item>
                        <Dropdown.Item onClick={this.handleDropSelect} value={36} className="dropdown_Item">36</Dropdown.Item>
                    </DropdownButton>
                    <Button
                        className="search_button_style"
                        onClick={this.onSearch}>
                        Search
                </Button>
                </div>
                <div className={errorClass}>Please review your search parameters</div>
            </React.Fragment>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        dispatch,
        ...bindActionCreators({ searchStream }, dispatch)
    }
}

export default connect(null, mapDispatchToProps)(SearchBar);
