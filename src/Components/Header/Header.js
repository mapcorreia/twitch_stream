import React from 'react'
import { Navbar } from 'react-bootstrap'
import './Header.css'

export const Header = (props) => {
    return (
        <Navbar className="header_style">
            <Navbar.Brand className="brand_style" href="/">Little Twitch</Navbar.Brand>
        </Navbar>

    )
}