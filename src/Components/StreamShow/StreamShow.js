import React from 'react'
import { Link } from 'react-router-dom';
import './StreamShow.css'
import ReactTwitchEmbedVideo from "react-twitch-embed-video"

const StreamShow = (props) => {
    const { viewer_count, title, user_name, source } = props.location.state

    return (
        <React.Fragment>
            <Link to="/" className="link_to_List">Return to Stream List</Link>
            <div className="videoPlayer_position container">
                <div className="videoPlayer_title">{title}</div>
                <ReactTwitchEmbedVideo channel={source} layout="video" />
                <div className="videoPlayer_footer">
                    <div className="videoPlayer_footer_username">Username: {user_name}</div>
                    <div className="spacer"></div>
                    <div className="videoPlayer_footer_views">Views:{viewer_count}</div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default StreamShow